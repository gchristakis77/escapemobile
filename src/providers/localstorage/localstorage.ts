import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/*
  Generated class for the LocalstorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalstorageProvider {
  SignUpData = { username:''};

  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello LocalstorageProvider Provider');
  }

  saveSettings(){
    this.storage.set('username', this.SignUpData.username).then((val) => {
      console.log('Your username is', val);
    });
  }

  getSettings(){
    this.storage.get('SignUpData.username').then((val) => {
      console.log('Your username is', val);
    });
  }

}
