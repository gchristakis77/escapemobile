import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

/**
 * Generated class for the CustomersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html',
})
export class CustomersPage {
  customers: Observable<any>

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.customers = this.getCustomers();
  }

  getCustomers(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post('https://escmobile.computermagic.gr/webService/Customers', 'username=' + encodeURIComponent('pitaridis') + '&password=' + encodeURIComponent('123'), {headers: headers} ).map(res => res.json());

  }

  openDetails(){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomersPage');
  }

}
