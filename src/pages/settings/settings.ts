import { LocalstorageProvider } from './../../providers/localstorage/localstorage';
import { Component } from '@angular/core';
import {IonicPage, NavController,  NavParams} from 'ionic-angular';
import { Storage } from '@ionic/storage';



/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  SignUpData = { username:''};

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {

     // set a key/value
    this.storage.get('SignUpData.username').then((val) => {
      console.log('Your username is', val);
    });

  }

  saveSettings(){
    console.log(this.SignUpData.username);
    this.storage.set('username', this.SignUpData.username).then((val) => {
      console.log('Your username is', val);
    });
  }



}
